package com.company;

public class QuizImplementation implements Quiz{

    private int digit;

    public QuizImplementation() {
        this.digit = 999;    // zmienna moze ulegac zmianie!
    }

    @Override
    public void isCorrectValue(int value) throws Quiz.ParamTooLarge, Quiz.ParamTooSmall {
        if (value > digit) {
            throw new Quiz.ParamTooLarge();
        } else if (value < digit) {
            throw new Quiz.ParamTooSmall();
        }
    }

}
