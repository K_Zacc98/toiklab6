package com.company;

public class Main {
    /**
     * metoda main powinna implementowac algorytm do
     * jak najszybszego wyszukiwania wartosci
     * zmiennej digit z klasy QuizImpl (zakladamy ze
     * programista nie zna zawartosci klasy QuizImpl).
     * Nalezy zalozyc, ze pole digit w klasie QuizImpl
     * moze w kazdej chwili ulec zmianie. Do wyszukiwania
     * odpowiedniej wartosci nalezy wykorzystywac tylko
     * i wylacznie metode isCorrectValue - jesli metoda
     * przestanie rzucac wyjatki wowczas mamy pewnosc ze
     * poszukiwana zmienna zostala odnaleziona.
     */


    public static void main(String[] args) {

        Quiz quiz = new QuizImplementation();
        int minValue = Quiz.MIN_VALUE;
        int maxValue = Quiz.MAX_VALUE;
        int digit = (minValue + maxValue) / 2;

        for (int counter = 1; ; counter++) {

            try {

                quiz.isCorrectValue(digit);
                System.out.println("YAAAAAAAASSS HONEY! Szukana liczba to: "
                        + digit + " Ilosc prob: " + counter);
                break;

            } catch (Quiz.ParamTooLarge e) {
                /* obsluga odpowiedniego wyjatku */
                System.out.println("Argument too big hun' ( ͡° ͜ʖ ͡°)");
                maxValue = digit;
                digit = (minValue + maxValue) / 2;
            } catch (Quiz.ParamTooSmall e) {
                /* obsluga odpowiedniego wyjatku */
                System.out.println("Argument too small ( ཀ ʖ̯ ཀ)");
                minValue = digit;
                digit = (minValue + maxValue) / 2;
            }
        }
    }
}
